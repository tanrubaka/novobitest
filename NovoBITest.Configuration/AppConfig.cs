﻿using System.Configuration;

namespace NovoBITest.Configuration
{
    public class AppConfig
    {
        public static string DefaultConnection =>
            ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    }
}
