﻿using NovoBITest.DAL.EntityModel;
using NovoBITest.DAL.IManagers;
using NovoBITest.DAL.Managers;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace NovoBITest.DAL
{
    public class BlModule : IModule
    {
        private readonly IUnityContainer _container;

        public BlModule(IUnityContainer container)
        {
            _container = container;
        }

        public void Initialize()
        {
            _container
                .RegisterType< ISalesStatisticManager, SalesStatisticManager>(new HierarchicalLifetimeManager())

                .RegisterType<NovoBITestEntities>(
                    new InjectionConstructor(new InjectionParameter(Configuration.AppConfig.DefaultConnection))); ;

        }
    }
}