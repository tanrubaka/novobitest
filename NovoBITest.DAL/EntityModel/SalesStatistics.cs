//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NovoBITest.DAL.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesStatistics
    {
        public string LowestLevel_Classifier_Type1 { get; set; }
        public string LowestLevel_Classifier_Type2 { get; set; }
        public string LowestLevel_Classifier_Type3 { get; set; }
        public string LowestLevel_Classifier_Type4 { get; set; }
        public System.DateTime ShipDate { get; set; }
        public decimal Amount { get; set; }
    }
}
