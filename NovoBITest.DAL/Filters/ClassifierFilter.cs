﻿namespace NovoBITest.DAL.Filters
{
    public class ClassifierFilter
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}