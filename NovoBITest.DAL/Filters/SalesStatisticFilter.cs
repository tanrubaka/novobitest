﻿using System.Collections.Generic;

namespace NovoBITest.DAL.Filters
{
    public class SalesStatisticFilter
    {
        private List<ClassifierFilter> _classifiers;

        public List<ClassifierFilter> Classifiers
        {
            get => _classifiers??(_classifiers = InitFilter());
            set => _classifiers = value;
        }

        private List<ClassifierFilter> InitFilter()
        {
            return new List<ClassifierFilter>
            {
                new ClassifierFilter{ Name = "LowestLevel_Classifier_Type1"},
                new ClassifierFilter{ Name = "LowestLevel_Classifier_Type2"},
                new ClassifierFilter{ Name = "LowestLevel_Classifier_Type3"},
                new ClassifierFilter{ Name = "LowestLevel_Classifier_Type4"},
            };
        }
    }
}