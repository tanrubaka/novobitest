﻿using System.Collections.Generic;
using NovoBITest.DAL.Filters;
using NovoBITest.DAL.Models;

namespace NovoBITest.DAL.IManagers
{
    public interface ISalesStatisticManager
    {
        IEnumerable<SalesStatisticInfo> Get(SalesStatisticFilter filter);
    }
}