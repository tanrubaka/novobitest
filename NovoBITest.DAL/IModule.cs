﻿namespace NovoBITest.DAL
{
    public interface IModule
    {
        void Initialize();
    }
}