﻿using NovoBITest.DAL.EntityModel;
using Unity.Attributes;

namespace NovoBITest.DAL.Managers
{
    public abstract class DataManagerBase
    {
        [Dependency]
        public NovoBITestEntities DataContext { get; set; }
    }
}