﻿using System.Collections.Generic;
using System.Linq;
using NovoBITest.DAL.EntityModel;
using NovoBITest.DAL.Filters;
using NovoBITest.DAL.IManagers;
using NovoBITest.DAL.Models;
using System.Linq.Dynamic;

namespace NovoBITest.DAL.Managers
{
    public class SalesStatisticManager: DataManagerBase, ISalesStatisticManager
    {
        public IEnumerable<SalesStatisticInfo> Get(SalesStatisticFilter filter)
        {
            var query = DataContext.SalesStatistics.AsQueryable();
            foreach (var filterClassifier in filter.Classifiers.Where(i=> !string.IsNullOrEmpty(i.Value)))
            {
                query = query.Where($"{filterClassifier.Name} == @0", filterClassifier.Value);
            }
            return GetInner(query)
                .ToList();
        }

        private IQueryable<SalesStatisticInfo> GetInner(IQueryable<SalesStatistics> query)
        {
            return query.Select(i => new SalesStatisticInfo
            {
                LowestLevelClassifierType1 = i.LowestLevel_Classifier_Type1,
                LowestLevelClassifierType2 = i.LowestLevel_Classifier_Type2,
                LowestLevelClassifierType3 = i.LowestLevel_Classifier_Type3,
                LowestLevelClassifierType4 = i.LowestLevel_Classifier_Type4,
                Amount = i.Amount,
                ShipDate = i.ShipDate
            });
        }
    }
}