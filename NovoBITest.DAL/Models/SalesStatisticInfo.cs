﻿namespace NovoBITest.DAL.Models
{
    public class SalesStatisticInfo
    {
        public string LowestLevelClassifierType1 { get; set; }
        public string LowestLevelClassifierType2 { get; set; }
        public string LowestLevelClassifierType3 { get; set; }
        public string LowestLevelClassifierType4 { get; set; }
        public System.DateTime ShipDate { get; set; }
        public decimal Amount { get; set; }

        public SalesStatisticInfo()
        {
            
        }

        public SalesStatisticInfo(SalesStatisticInfo obj)
        {
            this.Amount = obj.Amount;
            this.ShipDate = obj.ShipDate;
            this.LowestLevelClassifierType1 = obj.LowestLevelClassifierType1;
            this.LowestLevelClassifierType2 = obj.LowestLevelClassifierType2;
            this.LowestLevelClassifierType3 = obj.LowestLevelClassifierType3;
            this.LowestLevelClassifierType4 = obj.LowestLevelClassifierType4;
        }
    }
}