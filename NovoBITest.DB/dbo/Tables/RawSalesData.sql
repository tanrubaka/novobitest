﻿CREATE TABLE RawSalesData
(
	Channel NVARCHAR(50) NOT NULL, --Канал сбыта
	ProductId NVARCHAR(50) NOT NULL, --Товар
	CustomerId NVARCHAR(50) NOT NULL, --Клиент
	ShipDate DATE NOT NULL, --Дата продажи (отгрузки)
	Amount NUMERIC(28, 12) NOT NULL, --Объем
); 