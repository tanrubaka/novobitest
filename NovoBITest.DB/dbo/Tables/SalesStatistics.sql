﻿CREATE TABLE SalesStatistics
(
	LowestLevel_Classifier_Type1 NVARCHAR(50) NOT NULL, 
	LowestLevel_Classifier_Type2 NVARCHAR(50) NOT NULL,
	LowestLevel_Classifier_Type3 NVARCHAR(50) NOT NULL,
	LowestLevel_Classifier_Type4 NVARCHAR(50) NOT NULL,
	ShipDate DATE NOT NULL, --Дата продажи (отгрузки)
	Amount NUMERIC(28, 12) NOT NULL, --Объем
);