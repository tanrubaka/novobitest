﻿using System;

namespace NovoBITest.ViewModel
{
    public class ClosedEventArgs : EventArgs
    {
        public bool IsOk { get; set; }
    }
}