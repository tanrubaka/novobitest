﻿namespace NovoBITest.ViewModel
{
    public interface IViewModelManager
    {
        event ViewModelManager.ViewModelShowDelegate ViewModelShowEvent;
        event ViewModelManager.ViewModelShowDialogDelegate ViewModelShowDialogEvent;
        event ViewModelManager.ViewModelCloseDelegate ViewModelCloseEvent;

        void ViewModelShow(ViewModelBase viewModel);
        void ViewModelShowDialog(ViewModelBase viewModel);
        void ViewModelClose(ViewModelBase viewModel);
    }
}