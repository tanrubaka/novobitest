﻿using NovoBITest.DAL.Models;

namespace NovoBITest.ViewModel.Models
{
    public class SalesStatisticView : SalesStatisticInfo
    {
        public SalesStatisticView()
        {

        }

        public SalesStatisticView(SalesStatisticInfo obj) : base(obj)
        {

        }
    }
}