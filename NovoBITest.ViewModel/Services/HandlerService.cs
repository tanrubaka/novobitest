﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NovoBITest.ViewModel.Models;

namespace NovoBITest.ViewModel.Services
{
    public class HandlerService
    {
        public delegate string MergodDelegate(ObservableCollection<SalesStatisticView> salesStatistics);

        public Dictionary<string, MergodDelegate> Methods { get; set; }

        public HandlerService()
        {
            Methods = new Dictionary<string, MergodDelegate>
            {
                { Resources.HandlerService.ByDay, ByDay },
                { Resources.HandlerService.ByMonth, ByMonth },
                { Resources.HandlerService.ByQuarter, ByQuarter }
            };
        }

        private string ByDay(ObservableCollection<SalesStatisticView> salesStatistics)
        {
            return Resources.HandlerService.ByDayResult;
        }

        private string ByMonth(ObservableCollection<SalesStatisticView> salesStatistics)
        {
            return Resources.HandlerService.ByMonthResult;
        }

        private string ByQuarter(ObservableCollection<SalesStatisticView> salesStatistics)
        {
            return Resources.HandlerService.ByQuarterResult;
        }
    }
}