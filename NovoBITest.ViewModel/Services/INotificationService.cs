﻿namespace NovoBITest.ViewModel.Services
{
    public interface INotificationService
    {
        void ShowError(string message, string title);
        void ShowInfo(string message, string title);
        bool ShowDialog(string message, string title);
    }
}