﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using NovoBITest.DAL.Filters;
using NovoBITest.DAL.IManagers;
using NovoBITest.ViewModel.Models;
using NovoBITest.ViewModel.Services;
using Unity.Attributes;

namespace NovoBITest.ViewModel.ViewModel
{
    public class SalesStatisticsViewModel: ViewModelBase
    {
        private RelayCommand _getCommand;
        private RelayCommand _handleCommand;
        private ObservableCollection<SalesStatisticView> _salesStatistics;
        private string _result;
        private HandlerService.MergodDelegate _selectedMethod;

        [Dependency]
        public ISalesStatisticManager SalesStatisticManager { get; set; }

        [Dependency]
        public IViewModelManager ViewModelManager { get; set; }

        public SalesStatisticsViewModel()
        {
            this.Closed += delegate { ViewModelManager.ViewModelClose(this); };
        }

        public SalesStatisticFilter Filter { get; set; } = new SalesStatisticFilter();

        public HandlerService HandlerService { get; set; } = new HandlerService();

        public HandlerService.MergodDelegate SelectedMethod
        {
            get => _selectedMethod;
            set
            {
                _selectedMethod = value;
                HandleCommand.RaiseCanExecuteChanged();
            }
        }

        public string Result
        {
            get => _result;
            set
            {
                _result = value;
                OnPropertyChanged(nameof(Result));
            }
        }

        public ObservableCollection<SalesStatisticView> SalesStatistics
        {
            get => _salesStatistics;
            set
            {
                _salesStatistics = value;
                OnPropertyChanged(nameof(SalesStatistics));
                HandleCommand.RaiseCanExecuteChanged();
            }
        }

        public RelayCommand GetCommand => _getCommand ?? (_getCommand = new RelayCommand(OnGet));
        public RelayCommand HandleCommand => _handleCommand ?? (_handleCommand = new RelayCommand(OnHandle, EnableHandle));
        
        private async void OnGet()
        {
            SalesStatistics = await Task.Run(() =>
                new ObservableCollection<SalesStatisticView>(SalesStatisticManager.Get(Filter)
                .Select(i=> new SalesStatisticView(i))));
        }

        private bool EnableHandle()
        {
            return SelectedMethod != null && SalesStatistics!=null && SalesStatistics.Any();
        }

        private async void OnHandle()
        {
            Result = await Task.Run(() => SelectedMethod.Invoke(SalesStatistics));
        }
    }
}