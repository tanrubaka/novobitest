﻿using System.ComponentModel;
using GalaSoft.MvvmLight.Command;
using Unity;

namespace NovoBITest.ViewModel
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        private RelayCommand _closeCommand;

        public virtual string Header { get; set; }

        public IUnityContainer Container { get; set; }
        public delegate void ClosedViewModelHandler(object sender, ClosedEventArgs args);
        public event ClosedViewModelHandler Closed;


        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsClosed { get; set; }

        public void Close(ClosedEventArgs args)
        {
            if (!IsClosed && OnClosing())
            {
                IsClosed = true;
                OnClosed(args);
            }
        }

        protected virtual bool OnClosing()
        {
            return true;
        }

        private void OnClosed(ClosedEventArgs args)
        {
            ClosedViewModelHandler handler = Closed;
            handler?.Invoke(this, args);
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public RelayCommand CloseCommand => _closeCommand ?? (_closeCommand = new RelayCommand(OnClose));

        private void OnClose()
        {
            Close(new ClosedEventArgs { IsOk = true });
        }

    }
}