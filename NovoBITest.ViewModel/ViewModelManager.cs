﻿namespace NovoBITest.ViewModel
{
    public class ViewModelManager: IViewModelManager
    {
        public delegate void ViewModelShowDelegate(ViewModelBase viewModel);
        public delegate void ViewModelShowDialogDelegate(ViewModelBase viewModel);
        public delegate void ViewModelCloseDelegate(ViewModelBase viewModel);

        public event ViewModelShowDelegate ViewModelShowEvent;
        public event ViewModelShowDialogDelegate ViewModelShowDialogEvent;
        public event ViewModelCloseDelegate ViewModelCloseEvent;

        public void ViewModelShow(ViewModelBase viewModel)
        {
            OnViewModelShowEvent(viewModel);
        }

        public void ViewModelShowDialog(ViewModelBase viewModel)
        {
            ViewModelShowDialogEvent?.Invoke(viewModel);
        }

        public void ViewModelClose(ViewModelBase viewModel)
        {
            OnViewModelCloseEvent(viewModel);
        }

        private void OnViewModelCloseEvent(ViewModelBase viewModel)
        {
            ViewModelCloseEvent?.Invoke(viewModel);
        }

        private void OnViewModelShowEvent(ViewModelBase viewModel)
        {
            ViewModelShowEvent?.Invoke(viewModel);
        }
    }
}