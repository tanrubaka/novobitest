﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using NovoBITest.ViewModel;
using NovoBITest.ViewModel.Services;
using NovoBITest.ViewModel.ViewModel;
using NovoBITest1.Windows;
using Unity;
using Unity.Attributes;

namespace NovoBITest1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IUnityContainer _container = new UnityContainer();

        public App()
        {
            DispatcherUnhandledException += App_DispatcherUnhandledException;
        }

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            UnityConfig.RegisterComponents(_container);
            ViewModelManager = _container.Resolve<IViewModelManager>();
            ViewManager = _container.Resolve<IViewManager>();

            this.ViewModelManager.ViewModelShowEvent += ViewManager.ViewShow;
            this.ViewModelManager.ViewModelShowDialogEvent += ViewManager.ViewShowDialog;
            this.ViewModelManager.ViewModelCloseEvent += ViewManager.ViewClose;

            var salesStatisticsViewModel = _container.Resolve<SalesStatisticsViewModel>();

            var salesStatisticsWindow = _container.Resolve<SalesStatistics>();
            salesStatisticsWindow.DataContext = salesStatisticsViewModel;
            ViewModelManager.ViewModelShow(salesStatisticsViewModel);
        }

        void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            
            _container.Resolve<INotificationService>().ShowError(
                $"{e.Exception.Message} \n {e.Exception.InnerException}", "Error");
        }

        [Dependency]
        public INotificationService NotificationService { get; set; }

        [Dependency]
        public IViewModelManager ViewModelManager { get; set; }

        [Dependency]
        public IViewManager ViewManager { get; set; }
    }
}
