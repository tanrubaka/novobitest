﻿using NovoBITest.ViewModel;

namespace NovoBITest1
{
    public interface IViewManager
    {
        void ViewShow(ViewModelBase viewModel);
        void ViewShowDialog(ViewModelBase viewModel);
        void ViewClose(ViewModelBase viewModel);
    }
}