﻿using System.Windows;
using NovoBITest.ViewModel.Services;

namespace NovoBITest1
{
    public class NotificationService : INotificationService
    {
        public void ShowError(string message, string title)
        {
            Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void ShowInfo(string message, string title)
        {
            Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Show(string message, string title, MessageBoxButton button, MessageBoxImage image)
        {
            MessageBox.Show(message, title,
                button, image);
        }

        public bool ShowDialog(string message, string title)
        {
            MessageBoxResult result = MessageBox.Show(message, title,
                MessageBoxButton.YesNo, MessageBoxImage.Question);
            return result == MessageBoxResult.Yes;
        }
    }
}