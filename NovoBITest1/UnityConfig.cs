﻿using Microsoft.Win32;
using NovoBITest.ViewModel;
using NovoBITest.ViewModel.Services;
using Unity;
using Unity.Lifetime;

namespace NovoBITest1
{
    public class UnityConfig
    {
        public static IUnityContainer RegisterComponents(IUnityContainer container)
        {
            container.RegisterType<IViewManager, ViewManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IViewModelManager, ViewModelManager>(new HierarchicalLifetimeManager());
            
            container.RegisterType<INotificationService, NotificationService>(new HierarchicalLifetimeManager());

            NovoBITest.DAL.IModule dalModul = new NovoBITest.DAL.BlModule(container);
            dalModul.Initialize();
            return container;
        }
    }
}