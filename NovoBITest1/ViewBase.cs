﻿using System.ComponentModel;
using System.Windows;
using NovoBITest.ViewModel;

namespace NovoBITest1
{
    public class ViewBase : Window
    {
        public bool IsClosed { get; set; }
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            IsClosed = true;
            if (!((ViewModelBase)DataContext).IsClosed)
            {

                ((ViewModelBase)DataContext).Close(new ClosedEventArgs());
            }
        }
    }
}