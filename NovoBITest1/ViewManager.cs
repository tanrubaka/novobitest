﻿using System;
using System.Collections.Generic;
using System.Linq;
using NovoBITest.ViewModel;
using NovoBITest.ViewModel.ViewModel;
using NovoBITest1.Windows;

namespace NovoBITest1
{
    public class ViewManager: IViewManager
    {
        private ViewBase InnerShow(ViewModelBase viewModel)
        {
            if (!_mapping.ContainsKey(viewModel.GetType()))
            {
                throw new ArgumentException("Error mapping");
            }

            if (_openViewModel.ContainsKey(viewModel.GetType()))
            {
                throw new ArgumentException("The window is already open");
            }
            var view = (ViewBase)Activator.CreateInstance(_mapping[viewModel.GetType()]);
            view.DataContext = viewModel;
            return view;
        }

        public void ViewShow(ViewModelBase viewModel)
        {
            var view = InnerShow(viewModel);
            view.Show();
            _openViewModel.Add(viewModel.GetType(), view);
        }

        public void ViewShowDialog(ViewModelBase viewModel)
        {
            var view = InnerShow(viewModel);
            view.ShowDialog();
            _openViewModel.Add(viewModel.GetType(), view);
        }

        public void ViewClose(ViewModelBase viewModel)
        {
            if (_openViewModel.ContainsKey(viewModel.GetType()))
            {
                var view = _openViewModel[viewModel.GetType()];
                if (!view.IsClosed)
                {
                    view.Close();
                }
                _openViewModel.Remove(viewModel.GetType());
            }
            if (!_openViewModel.Any())
            {
                App.Current.Shutdown();
            }
        }


        private readonly Dictionary<Type, Type> _mapping =
            new Dictionary<Type, Type>
            {
                {typeof(SalesStatisticsViewModel), typeof(SalesStatistics)},
            };

        private readonly Dictionary<Type, ViewBase> _openViewModel = new Dictionary<Type, ViewBase>();


    }
}