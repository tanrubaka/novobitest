﻿update t1
set t1.Channel = 
 (select top 1 t2.Channel 
	from RawSalesData t2 where t2.CustomerId = t1.CustomerId
	group by t2.Channel
	order by sum(t2.Amount) desc)
from RawSalesData t1